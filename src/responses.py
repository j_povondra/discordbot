
class BotResponses:
    @staticmethod
    def camera_set_on():
        return "🟢 Camera set ON"

    @staticmethod
    def camera_set_off():
        return "🔴 Camera set OFF"

    @staticmethod
    def unauthorized(user):
        return f"⚠️ User {user} unknown"

    @staticmethod
    def permission_denied():
        return "⚠️ Permission denied"

    @staticmethod
    def whoiam(name, id, role):
        return [f'NAME: {name}', f'ID: {id}', f'INTERNAL-BOT-ROLE: {role}']
