import discord
from discord.ext import commands

from src.settings import PREFIX


intents = discord.Intents.default()
intents.message_content = True
client = commands.Bot(command_prefix=PREFIX, intents=intents)