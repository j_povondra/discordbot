import aiohttp

from src.bot import client
from src.functions.utils import get_ip
from src.responses import BotResponses
from src.settings import SECRETS


@client.command()
async def whoiam(ctx):
    name = ctx.author
    id = ctx.author.id
    role = "Admin" if ctx.author.id in SECRETS.ADMIN_IDS else "User"

    for row in BotResponses.whoiam(
            id=id,
            name=name,
            role=role
    ):
        await ctx.send(row)


@client.command()
async def access_roulette(ctx):
    ip = get_ip()
    user = await client.fetch_user(ctx.author.id)

    async with aiohttp.ClientSession() as session:
        async with session.post(f"http://{ip}/register/user/", data={"username": str(ctx.author), "origin": "botapi"}) as response:
            if response.status != 200:
                await user.send(f"Failed communication with roulette server")
                return
            json_response = await response.json()
            link = json_response["link"]
            await user.send(f"Your account access key http://{ip}{link}")
