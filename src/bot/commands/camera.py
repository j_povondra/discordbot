import asyncio

from src.bot import client
from src.settings import SECRETS
from src.responses import BotResponses
from src.functions.control_camera import set_camera_on, set_camera_off
from src.functions.notification import notify_admin
from src.admin_notifications import AdminNotification


@client.command()
async def camera_on(ctx):

    if not ctx.message.author.id in SECRETS.ADMIN_IDS:
        await ctx.send(BotResponses.permission_denied())
        asyncio.create_task(
            notify_admin(
                AdminNotification.command_abuse_by(
                    ctx.message.author,
                    "camera_on"
                )
            )
        )
    else:
        set_camera_on()
        await ctx.send(BotResponses.camera_set_on())
        asyncio.create_task(
            notify_admin(
                AdminNotification.camera_was_set_on_by(
                    ctx.message.author,
                )
            )
        )

@client.command()
async def camera_off(ctx):

    if not ctx.message.author.id in SECRETS.ADMIN_IDS:
        await ctx.send(BotResponses.permission_denied())
        asyncio.create_task(
            notify_admin(
                AdminNotification.command_abuse_by(
                    ctx.message.author,
                    "camera_off"
                )
            )
        )
    else:
        set_camera_off()
        await ctx.send(BotResponses.camera_set_off())
        asyncio.create_task(
            notify_admin(
                AdminNotification.camera_was_set_off_by(
                    ctx.message.author,
                )
            )
        )
