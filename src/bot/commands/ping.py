from src.bot import client


@client.command()
async def ping(ctx):
	await ctx.send(f"I am alive with latency {round(client.latency * 1000)}ms")
