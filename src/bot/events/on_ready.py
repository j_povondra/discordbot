import discord

from src.bot import client
from src.settings import VERSION
from src.constants import Channels


@client.event
async def on_ready():
    activity = discord.Game(name=f'@v{VERSION}')

    await client.change_presence(status=discord.Status.online, activity=activity)

    admin_channel = client.get_channel(Channels.ADMIN.value)

    await admin_channel.send(f'🟢 Started with code version {VERSION}')
