from src.bot import client
from src.settings import NOTIFIED_ADMIN_ID, ADMIN_NOTIFICATIONS_ENABLED


async def notify_admin(message):
    if ADMIN_NOTIFICATIONS_ENABLED:
        user = await client.fetch_user(NOTIFIED_ADMIN_ID)
        await user.send(message) if user else False
