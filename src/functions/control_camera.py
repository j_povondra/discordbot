from pytapo import Tapo

from src.settings import SECRETS


def set_camera_on():
    tapo = Tapo(
        SECRETS.CAMERA_HOST,
        SECRETS.CAMERA_USER,
        SECRETS.CAMERA_PASSWORD
    )
    tapo.setPrivacyMode(False)

def set_camera_off():
    tapo = Tapo(
        SECRETS.CAMERA_HOST,
        SECRETS.CAMERA_USER,
        SECRETS.CAMERA_PASSWORD
    )
    tapo.setPrivacyMode(True)
