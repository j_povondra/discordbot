
class AdminNotification:

    @staticmethod
    def camera_was_set_on_by(user):
        return f"🟢 Camera set **ON** by {user}"

    @staticmethod
    def camera_was_set_off_by(user):
        return f"🔴 Camera set **OFF** by {user}"

    @staticmethod
    def command_abuse_by(user, command):
        return f"⚠️**{user}** abused command **{command}** resulting permission denied."
