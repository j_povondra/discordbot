FROM python:3.10-slim

WORKDIR /app

ENV PYTHONUNBUFFERED=1

RUN apt -y update && apt -y install curl

COPY . .

RUN pip install --upgrade pip wheel

RUN pip install -r requirements.txt
