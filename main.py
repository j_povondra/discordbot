# General imports
from src.bot import client
from src.settings import SECRETS

# Import commands here
from src.bot.commands.camera import camera_on, camera_off # noqa
from src.bot.commands.ping import ping # noqa
from src.bot.commands.admin import whoiam # noqa

# Import events here
from src.bot.events.on_ready import on_ready # noqa


if __name__ == '__main__':
    client.run(SECRETS.BOT_KEY)
